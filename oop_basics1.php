<?php
    class StudentInfo {
    public $std_id="";
    public $std_name="";
    public $std_cgpa=0.00;

        public function set_id($std_id){

            $this->std_id = $std_id;
            //use this to identify global variable , not local variable

            echo $this->std_id;
    }
}

$obj = new StudentInfo;
//if we pass any parameter , then we use parenthesis , otherwise it is not necessary
$obj->set_id("SEIP_148423");
?>
<br>
<br>



<?php
    class StudentInformation{
    public $std_id = "";
    public $std_name = "";
    public $std_cgpa = 0.00;
}

$obj = new StudentInformation;
$obj->std_id= "SEIP148423";

echo $obj->std_id;
?>
<br>
<br>



<?php
    class StdInfo{
        public $std_id = "";
        public $std_name = "";
        public $std_cgpa = 0.00;

        public function set_std_id($std_id){
            $this->std_id = $std_id;
    }

        public function set_std_name($std_name){
            $this->std_name = $std_name;
    }

        public function set_std_cgpa($std_cgpa){
            $this->std_cgpa = $std_cgpa;
    }

        public function get_std_id(){
            return $this->std_id;
    }

        public function get_std_name(){
            return $this->std_name;
    }

        public function get_std_cgpa(){
            return $this->std_cgpa;
    }
}
//when we use multiple instances

$obj = new StdInfo;


$obj->set_std_id("148423");
$obj->set_std_name("afsana");
$obj->set_std_cgpa("3.90");

echo $obj->get_std_id()."<br>";
echo $obj->get_std_name()."<br>";
echo $obj->get_std_cgpa()."<br>";
?>
<br>
<br>



<?php
class StdInfomtn{
    public $std_id = "";
    public $std_name = "";
    public $std_cgpa = 0.00;

    public function set_std_id($std_id){
        $this->std_id = $std_id;
    }

    public function set_std_name($std_name){
        $this->std_name = $std_name;
    }

    public function set_std_cgpa($std_cgpa){
        $this->std_cgpa = $std_cgpa;
    }

    public function get_std_id(){
        return $this->std_id;
    }

    public function get_std_name(){
        return $this->std_name;
    }

    public function get_std_cgpa(){
        return $this->std_cgpa;
    }

    public function ShowDetails(){
        echo "Student ID: " .$this->std_id."<br>";
        echo "Student Name: " .$this->std_name."<br>";
        echo "Student CGPA: " .$this->std_cgpa."<br>";
    }
}
//when we use multiple instances

$obj = new StdInfomtn;


$obj->set_std_id("148423");
$obj->set_std_name("afsana");
$obj->set_std_cgpa("3.90");

$obj->ShowDetails();
?>
