<?php
class StudentInfo{
    public $justOneVariable = "Hello BITM!";
    public function justOneMethod()
    {
       echo "Hello World!";
    }
    public function __construct()
    {
        echo $this->justOneVariable;
    }
    public function __destruct()
    {
        echo "Good Bye ... : ( ";
    }
}
$justOneObject = new StudentInfo;
unset($justOneObject);//for unset justOneObject
$justOneObject->justOneMethod();
?>




